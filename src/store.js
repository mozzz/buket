import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		activeCategory: 1,
		menuVisible: false,
		categories: [
			{
				id: 1,
				name: 'Букеты',
			},
			{
				id: 2,
				name: 'Розы',
			},
			{
				id: 3,
				name: 'Композиции',
			},
		],
		products:[]
	},
	mutations: {
		setProducts(state,products){
			state.products = products;
		},
		setCategory(state,id){
			state.activeCategory = id;
			
		},
		menuToggle(state) {
			state.menuVisible = !state.menuVisible;
		},
		menuClose(state) {
			state.menuVisible = false;
		}
	},
	actions: {
		getProducts({commit, state}) {
			let url= './assets/category_'+state.activeCategory+'.json';
			axios.get(url).then(function (response) {
				
				// handle success
				// console.log(response.data);
				commit('setProducts', response.data);
			})
			.catch(function (error) {
				// handle error
				console.log(error);
			})
			.then(function () {
				// always executed
			});
		},
		setCategory({commit,state,dispatch},id){
			if (state.activeCategory == id) {
				return;
			}
			commit('setCategory', id);
			dispatch('getProducts');
		},
		
	},
});
